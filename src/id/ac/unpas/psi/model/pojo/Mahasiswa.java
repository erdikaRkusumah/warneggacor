/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.unpas.psi.model.pojo;

/**
 *
 * @author Erdika Rhamadan K
 */
public class Mahasiswa {
    private String nrp;
    private String nama;
    private float ipk;
    
    public Mahasiswa(){
        
    }
    
    public  Mahasiswa(String nrp, String nama, float ipk){
        this.nama = nama;
        this.nrp = nrp;
        this.ipk = ipk;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setIpk(float ipk) {
        this.ipk = ipk;
    }

    public String getNrp() {
        return nrp;
    }

    public String getNama() {
        return nama;
    }

    public float getIpk() {
        return ipk;
    }
    
    
}
