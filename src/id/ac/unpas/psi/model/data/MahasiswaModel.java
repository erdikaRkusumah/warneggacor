/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.unpas.psi.model.data;

import id.ac.unpas.psi.model.pojo.Mahasiswa;
import id.ac.unpas.psi.utilities.DatabaseUtilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Erdika Rhamadan K
 */
public class MahasiswaModel {
    public List<Mahasiswa> loadMahasiswa() throws  SQLException{
        List<Mahasiswa> mhsList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT * FROM mahasiswa");
            mhsList = new ArrayList<>();
            while (rs.next()) {                
                Mahasiswa mhs = new Mahasiswa();
                mhs.setNrp(rs.getString("nrp"));
                mhs.setNama(rs.getString("nama"));
                mhs.setIpk(rs.getFloat("ipk"));
                
                mhsList.add(mhs);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return  mhsList;
    }
    
    public int save(Mahasiswa mhs) throws SQLException{
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("INSERT INTO mahasiswa values (?,?,?)");
            stat.setString(1, mhs.getNrp());
            stat.setString(2, mhs.getNama());
            stat.setFloat(3, mhs.getIpk());
            
            return stat.executeUpdate();
        } finally{
            if (con != null) {
                con.close();
            }
        }
    }
    
    public int delete(String nrp) throws SQLException{
        Connection con = DatabaseUtilities.getConnection();
        
        try {
            PreparedStatement stat = con.prepareStatement("DELETE FROM mahasiswa WHERE nrp = '" + nrp + "'");
            stat.executeUpdate();
            
            return stat.executeUpdate();
        } finally{
            if (con != null) {
                con.close();
            }
        }
    }
    
    public int update(String nrp, String nama, Float ipk) throws SQLException{
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("UPDATE mahasiswa set nama='"+nama+"', ipk='"+ipk+"' WHERE nrp='"+ nrp+"'");
            return stat.executeUpdate();
        } finally{
            if (con != null) {
                con.close();
            }
        }
    }
}