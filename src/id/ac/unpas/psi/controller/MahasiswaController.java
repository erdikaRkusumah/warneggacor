/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.unpas.psi.controller;

import id.ac.unpas.psi.model.data.MahasiswaModel;
import id.ac.unpas.psi.model.pojo.Mahasiswa;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Erdika Rhamadan K
 */
public class MahasiswaController {
    public  List<Mahasiswa> loadMahasiswas() throws  SQLException{
        MahasiswaModel model = new MahasiswaModel();
        return model.loadMahasiswa();
    }
    
    public int insert(Mahasiswa mahasiswa) throws SQLException{
        MahasiswaModel model = new MahasiswaModel();
        return model.save(mahasiswa);
    }
    
    public int delete(String nrp) throws SQLException{
        MahasiswaModel model = new MahasiswaModel();
        return model.delete(nrp);
    }
    
    public int update(String nrp, String nama, Float ipk) throws SQLException{
        MahasiswaModel model = new MahasiswaModel();
        return model.update(nrp, nama, ipk);
    }
}
